package com.gitlab.lemon1806.specialtablist;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.protocol.wrappers.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.stream.Collectors;

public class SpecialTabList extends JavaPlugin implements Listener {

    private final Map<Integer, UUID> dummyUUID = new HashMap<>();

    @Override
    public void onEnable() {
        for (int loop = 0; loop <= 59; loop++) {
            dummyUUID.put(loop, UUID.randomUUID());
        }

        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        new BukkitRunnable() {
            @Override
            public void run() {
                initTabList(player);

                Bukkit.getOnlinePlayers().forEach(online -> removePlayer(player, online));
            }
        }.runTaskLater(this, 1);

        new BukkitRunnable() {
            @Override
            public void run() {
                updateTabList(player);
            }
        }.runTaskTimer(this, 1, 20);
    }

    private void initTabList(Player player) {
        removeAllPlayers(player);

        List<PlayerInfoData> emptyData = new ArrayList<>();

        for (int loop = 0; loop <= 59; loop++) {
            emptyData.add(getDummyData(loop, "§r", "eyJ0aW1lc3RhbXAiOjE1MTA4Mjk0Mjk1ODYsInByb2ZpbGVJZCI6ImFkMWM2Yjk1YTA5ODRmNTE4MWJhOTgyMzY0OTllM2JkIiwicHJvZmlsZU5hbWUiOiJGdXJrYW5iejAwIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9mMWRhNGNjZGQ2MzEyODkwNTFlMzU5YjI0ODcxM2M0ZTZjMWY4ZmVjYzhkZGNkYjk5ZTg4ZDZkOWY2N2Y5ZWYifX19", "grZzxhUx8Y/erYXZ3L8k4Zgg7pbppQ7nWGfS2yeLUiR2d1qAef6mFiKUVJGKKiDV/nBbQ4WZJQ/+aMIUBnVJKKfxScjP3lMSben475l/4Ug9Zr/fJB2B2xNEn6AT5YRR6bJGx3IrzSz5ZrVPXJqQi6r3igihKG1ZzyiBgWCPMqfE6cqzjA73IJqhr8RmTRW/VorbkB+Hmv4ZYS9vA5p397gJYqQUYOsOlJhUMBhZ4MTiPZKBIrg7RcWMtjmYMFXG1dM7supDqwpmgkVG8WEL7H2uq60kEygiq+SCF/zh9gdqGmf65wIWpyhbba96r0lyWB4iEHPOCCF1NCScxQGKZFlRmtuYN9/w5wsDFJHuFKFMEjf5AMnB4hNYLIBkJYr66Ld62jT5uaRxgaV5ivfQsv+6Wvzx711i5xSLPN4sb7jZhvaQy5Koyurp7QYioVw9QoJZEWkYI8ixGa/c35a0utn9WFs9Fv4XeBGkpdIrPCnsq9cM9K6fhJFOFS0WXlpQJngqNQIkjMv4JIgdrhmjZc9VoZu9eru3h7f4Ii/fYbsp6PNuY2+oUlGGvz6QIkY5yvuhcMCE6qLYU//nxkJjfzNaWkRXCBjjfZ5aOK86PDJZ5xtOXbHsK3UcOTm46LUEHsbw1lBfXP+tYW69LHmbLjuiT5RT7ymU1k/0t0NINu8="));
        }

        sendAddPacket(emptyData, player);

        updateLine(0, "§bプレイヤー", "§c体力", "§a座標", player);
    }

    private void updateTabList(Player player) {
        int line = 1;

        for (Player online : Bukkit.getOnlinePlayers()) {
            updateLine(line, "§b" + online.getName(), "§c" + getHealth(online), "§a" + getLocation(online), player);
            line++;
        }

        while (line <= 19) {
            updateLine(line, "§r", "§r", "§r", player);
            line++;
        }
    }

    private void updateLine(int line, String first, String second, String third, Player player) {
        List<PlayerInfoData> list = new ArrayList<>();
        list.add(getDummyData(line, first, "", ""));
        list.add(getDummyData(line + 20, second, "", ""));
        list.add(getDummyData(line + 40, third, "", ""));
        sendUpdatePacket(list, player);
    }

    private void removePlayer(Player target, Player player) {
        sendRemovePacket(Collections.singletonList(getPlayerData(target)), player);
    }

    private void removeAllPlayers(Player player) {
        sendRemovePacket(Bukkit.getOnlinePlayers().stream().map(this::getPlayerData).collect(Collectors.toList()), player);
    }

    private void sendAddPacket(List<PlayerInfoData> data, Player player) {
        sendInfoPacket(data, EnumWrappers.PlayerInfoAction.ADD_PLAYER, player);
    }

    private void sendRemovePacket(List<PlayerInfoData> data, Player player) {
        sendInfoPacket(data, EnumWrappers.PlayerInfoAction.REMOVE_PLAYER, player);
    }

    private void sendUpdatePacket(List<PlayerInfoData> data, Player player) {
        sendInfoPacket(data, EnumWrappers.PlayerInfoAction.UPDATE_DISPLAY_NAME, player);
    }

    private void sendInfoPacket(List<PlayerInfoData> data, EnumWrappers.PlayerInfoAction action, Player player) {
        WrapperPlayServerPlayerInfo info = new WrapperPlayServerPlayerInfo();
        info.setAction(action);
        info.setData(data);
        info.sendPacket(player);
    }

    private PlayerInfoData getPlayerData(Player player) {
        WrappedGameProfile profile = WrappedGameProfile.fromPlayer(player);
        int latency = 0;
        EnumWrappers.NativeGameMode gameMode = EnumWrappers.NativeGameMode.fromBukkit(player.getGameMode());
        WrappedChatComponent displayName = WrappedChatComponent.fromText(profile.getName());

        return new PlayerInfoData(profile, latency, gameMode, displayName);
    }

    private PlayerInfoData getDummyData(int listId, String listName, String value, String signature) {
        WrappedGameProfile profile = new WrappedGameProfile(dummyUUID.get(listId), String.format("%03d", listId));
        profile.getProperties().put("textures", new WrappedSignedProperty("textures", value, signature));
        int latency = 0;
        EnumWrappers.NativeGameMode gameMode = EnumWrappers.NativeGameMode.SURVIVAL;
        WrappedChatComponent displayName = WrappedChatComponent.fromText(listName);

        return new PlayerInfoData(profile, latency, gameMode, displayName);
    }

    private String getHealth(Player player) {
        return (int) player.getHealth() / 2 + "❤";
    }

    private String getLocation(Player player) {
        Location location = player.getLocation();
        return "X:" + location.getBlockX() + " Y:" + location.getBlockY() + " Z:" + location.getBlockZ();
    }

}

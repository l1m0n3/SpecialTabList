# SpecialTabList [![pipeline status](https://gitlab.com/Lemon1806/SpecialTabList/badges/master/pipeline.svg)](https://gitlab.com/Lemon1806/SpecialTabList/commits/master)

Tabにプレイヤー、体力、座標を表示するBukkit/Spigotプラグインです。

このプラグインは[ProtocolLib](https://www.spigotmc.org/resources/protocollib.1997/)を使用しています。

[SpecialTabListのダウンロード](https://gitlab.com/Lemon1806/SpecialTabList/-/jobs/artifacts/master/raw/target/SpecialTabList.jar?job=build)
